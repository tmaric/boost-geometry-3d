
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/multi/geometries/multi_polygon.hpp>

#include <fstream>
#include <vector>

using namespace boost::geometry;
using namespace std;
    
typedef model::point<double, 3, cs::cartesian> Point;
typedef model::polygon<Point> Polygon;
typedef model::ring<Point> Ring;

typedef model::multi_polygon<Polygon, std::vector> MultiPolygon;

// FIXME: write counts on the polyhedron_tag being available, extend tags. 
namespace boost { namespace geometry {  

    struct polyhedron_tag {}; 

    namespace traits { 

        template<>
        struct tag<MultiPolygon>
        {
            typedef polyhedron_tag type;  
        };

} } } // namespace geometry::traits


// Path to the boost VTK io
//#include <boost/geometry/io/vtk/write.hpp>

// Compiling with cmake. 
#include "write.hpp"

int main(int argc, const char *argv[])
{
    Ring ring;
    ring.push_back(Point(0,0,0)); 
    ring.push_back(Point(1,0,0)); 
    ring.push_back(Point(1,1,0)); 
    ring.push_back(Point(0,1,0)); 

    ofstream ringFile; 
    ringFile.open("ring-type-tag.vtk"); 

    ringFile << vtk(ring);

    Polygon p1;
    Polygon::ring_type&  p1ring = p1.outer(); 
    p1ring.push_back(Point(0,0,0)); 
    p1ring.push_back(Point(1,0,0)); 
    p1ring.push_back(Point(0,1,0)); 

    ofstream polygonFile; 
    polygonFile.open("polygon-type-tag.vtk"); 

    polygonFile << vtk(p1);

    MultiPolygon m; 

    Polygon p2; 
    Polygon::ring_type& p2ring = p2.outer(); 
    p2ring.push_back(Point(0,0,0));
    p2ring.push_back(Point(0,0,1));
    p2ring.push_back(Point(1,0,0));

    Polygon p3; 
    Polygon::ring_type& p3ring = p3.outer(); 
    p3ring.push_back(Point(0,0,0));
    p3ring.push_back(Point(0,1,0));
    p3ring.push_back(Point(0,0,1));

    Polygon p4; 
    Polygon::ring_type& p4ring = p4.outer(); 
    p4ring.push_back(Point(1,0,0));
    p4ring.push_back(Point(0,1,0));
    p4ring.push_back(Point(0,0,1));

    m.push_back(p1);
    m.push_back(p2);
    m.push_back(p3);
    m.push_back(p4);

    ofstream multiPolygonFile;

    multiPolygonFile.open("multipolygon-type-tag.vtk");
    multiPolygonFile << vtk(m); 

    return 0;
}
