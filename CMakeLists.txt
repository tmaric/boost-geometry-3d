cmake_minimum_required (VERSION 2.8)

project (GEOMETRY3D)

set (EXECUTABLE_OUTPUT_PATH ${GEOMETRY3D_SOURCE_DIR}/build/bin)

add_subdirectory (libs/geometry/extensions/test/io/vtk)
