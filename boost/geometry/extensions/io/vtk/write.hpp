// Boost.Geometry (aka GGL, Generic Geometry Library)

// Copyright (c) 2007-2012 Barend Gehrels, Amsterdam, the Netherlands.
// Copyright (c) 2008-2012 Bruno Lalande, Paris, France.
// Copyright (c) 2009-2012 Mateusz Loskot, London, UK.

// Parts of Boost.Geometry are redesigned from Geodan's Geographic Library
// (geolib/GGL), copyright (c) 1995-2010 Geodan, Amsterdam, the Netherlands.

// Use, modification and distribution is subject to the Boost Software License,
// Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_GEOMETRY_IO_VTK_WRITE_HPP
#define BOOST_GEOMETRY_IO_VTK_WRITE_HPP

#include <sstream>

#include <ostream>
#include <string>

#include <boost/array.hpp>
#include <boost/concept/assert.hpp>
#include <boost/range.hpp>
#include <boost/typeof/typeof.hpp>

#include <boost/geometry/algorithms/assign.hpp>
#include <boost/geometry/algorithms/convert.hpp>
#include <boost/geometry/core/exterior_ring.hpp>
#include <boost/geometry/core/interior_rings.hpp>
#include <boost/geometry/core/ring_type.hpp>

#include <boost/geometry/geometries/concepts/check.hpp>
#include <boost/geometry/geometries/ring.hpp>

#include <boost/geometry/io/wkt/detail/prefix.hpp>


//#include "write.hpp"

namespace boost { namespace geometry {

    namespace detail { namespace vtk { 

        template <typename P, int I, int Count>
        struct stream_coordinate
        {
            template <typename Char, typename Traits>
            static inline void apply(std::basic_ostream<Char, Traits>& os, P const& p)
            {
                os << (I > 0 ? " " : "") << get<I>(p);
                // Uses metaprogramming instead of loops.
                stream_coordinate<P, I + 1, Count>::apply(os, p);
            }
        };

        template <typename P, int Count>
        struct stream_coordinate<P, Count, Count>
        {
            template <typename Char, typename Traits>
            static inline void apply(std::basic_ostream<Char, Traits>&, P const&)
            {}
        };

        /*!
        \brief Stream points as \ref VTK : for VTK points there is no Prefix Policy
        */
        template <typename Point>
        struct vtk_point
        {
            template <typename Char, typename Traits>
            static inline void apply(std::basic_ostream<Char, Traits>& os, Point const& p)
            {
                stream_coordinate<Point, 0, dimension<Point>::type::value>::apply(os, p);
            }
        };

        template <typename Ring>
        struct vtk_ring
        {
            template <typename Char, typename Traits>
            static inline void apply(std::basic_ostream<Char, Traits>& os,
                        Ring const& ring)
            {
                typedef typename point_type<Ring>::type point_type;
                typename Ring::const_iterator it;

                os << "# vtk DataFile Version 2.0\npolygon\nASCII\nDATASET POLYDATA\n"
                    << "POINTS " << ring.size() << " float\n"; 

                for (it = ring.begin(); it != ring.end(); ++it)
                {
                    stream_coordinate
                        <
                            point_type, 0, dimension<point_type>::type::value
                        >::apply(os, *it);

                    // This comes from the per-point first coordinate ternary operator
                    // test in the vtk_point struct in detail::.  
                    os << " "; 
                } 

                os << "\nPOLYGONS 1 " << ring.size() + 1 << "\n" << ring.size(); 

                for (typename Ring::size_type I = 0; I < ring.size(); ++I)
                {
                    os << " " << I;  
                }
            }
        };

        template <typename Polygon>
        struct vtk_polygon
        {
            template <typename Char, typename Traits>
            static inline void apply(std::basic_ostream<Char, Traits>& os,
                        Polygon const& polygon)
            {
                typedef typename point_type<Polygon>::type point_type;

                typedef typename Polygon::ring_type Ring; 

                // VTK supports only polygons without holes. 
                Ring const & ring = polygon.outer();
                vtk_ring<Ring>::apply(os, ring); 
            }
        };

        template <typename Polyhedron>
        struct vtk_polyhedron
        {
            template <typename Char, typename Traits>
            static inline void apply(std::basic_ostream<Char, Traits>& os,
                        Polyhedron const& polyhedron)
            {
                typedef typename Polyhedron::value_type Polygon; 
                typedef typename point_type<Polygon>::type point_type;

                os << "# vtk DataFile Version 2.0\npolyhedron\nASCII\nDATASET POLYDATA\n"; 
                std::stringstream pointStream; 
                std::stringstream polygonStream;

                typename Polyhedron::const_iterator polygonIt; 

                unsigned long nPoints = 0; 
                unsigned long nPointsOld = 0;
                unsigned long vtkPolygonCount = 0;

                for (polygonIt = polyhedron.begin(); polygonIt != polyhedron.end(); 
                        ++polygonIt)
                {
                    typename Polygon::ring_type const & ring = polygonIt->outer(); 
                    typename Polygon::ring_type::const_iterator pointIt; 

                    polygonStream << ring.size() << " "; 
                    unsigned long nPolygonPoints = 0;

                    for (
                            pointIt = ring.begin(); 
                            pointIt != ring.end(); 
                            ++pointIt, 
                            ++nPoints, 
                            ++nPolygonPoints
                        )
                    {
                        stream_coordinate
                            <
                                point_type, 0, dimension<point_type>::type::value
                            >::apply(pointStream, *pointIt);

                        // TODO: fix this
                        pointStream << " "; 

                        polygonStream << nPolygonPoints + nPointsOld << " "; 
                    }

                    vtkPolygonCount += 1 + nPolygonPoints;
                    nPointsOld = nPoints;

                    pointStream << "\n";
                    polygonStream << "\n";
                }

                os << "POINTS " << nPoints  << " float\n" << pointStream.str() 
                    << "\nPOLYGONS " << polyhedron.size() << " " 
                    << vtkPolygonCount << "\n"
                    << polygonStream.str();
                
            }
        };

    }} // namespace detail::vtk

    namespace dispatch {

        template <typename Tag, typename Geometry>
        struct vtk 
        {
           BOOST_MPL_ASSERT_MSG
                (
                    false, NOT_YET_IMPLEMENTED_FOR_THIS_GEOMETRY_TYPE
                    , (types<Geometry>)
                );
        };

        template <typename Point>
        struct vtk<point_tag, Point>
            : detail::vtk::vtk_point 
                <
                    Point
                >
        {};

        template <typename Ring>
        struct vtk<ring_tag, Ring>
            : detail::vtk::vtk_ring
                <
                    Ring 
                >
        {};

        template <typename Polygon>
        struct vtk<polygon_tag, Polygon>
            : detail::vtk::vtk_polygon
                <
                    Polygon 
                >
        {};

        template <typename Polyhedron>
        struct vtk<polyhedron_tag, Polyhedron>
            : detail::vtk::vtk_polyhedron
                <
                    Polyhedron 
                >
        {};

    } // namespace dispatch

    template <typename Geometry>
    class vtk_manipulator
    {
        public:

            inline vtk_manipulator(Geometry const& g)
                : m_geometry(g)
            {}

            template <typename Char, typename Traits>
            inline friend std::basic_ostream<Char, Traits>& operator<<(
                    std::basic_ostream<Char, Traits>& os,
                    vtk_manipulator const& m)
            {
                dispatch::vtk
                    <
                        typename tag<Geometry>::type,
                        Geometry
                    >::apply(os, m.m_geometry);
                os.flush(); 
                return os;
            }

        private:
            Geometry const& m_geometry;
    };

    template <typename Geometry>
    inline vtk_manipulator<Geometry> vtk(Geometry const& geometry)
    {
        concept::check<Geometry const>();

        return vtk_manipulator<Geometry>(geometry);
    }

}}

#endif
